package com.example.jobscheduler.log;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class LogRepository {

    private static final String ARG_LOG_KEY = "com.example.jobscheduler.ARG_LOG_KEY";

    private final SharedPreferences sharedPreferences;

    public LogRepository(SharedPreferences sharedPreferences) {
        this.sharedPreferences = sharedPreferences;
    }

    public void log(String log) {
        List<String> logs = list();
        ArrayList<String> data = new ArrayList<String>(logs);
        Date now = new Date();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("DD.MM HH:mm:ss", Locale.getDefault());
        String nowDate = simpleDateFormat.format(now);
        data.add(nowDate+ ": " +log);
        sharedPreferences.edit().putString(ARG_LOG_KEY, new Gson().toJson(data, new TypeToken<ArrayList<String>>(){}.getType())).apply();
    }

    public List<String> list() {
        return Arrays.asList(new Gson().fromJson(sharedPreferences.getString(ARG_LOG_KEY, "[]"), String[].class));
    }

}
