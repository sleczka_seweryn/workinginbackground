package com.example.jobscheduler.service.background;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.example.jobscheduler.app.JobSchedulerApplication;
import com.example.jobscheduler.service.ServiceManager;

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        JobSchedulerApplication app = JobSchedulerApplication.get(context);
        ServiceManager serviceManager = app.provideServiceManager();
        String action = intent.getAction();
        if (Intent.ACTION_BOOT_COMPLETED.equals(action) && serviceManager.isScheduled()) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                serviceManager.schedule();
            }
        }
    }
}
