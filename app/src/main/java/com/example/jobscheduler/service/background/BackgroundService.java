package com.example.jobscheduler.service.background;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

import com.example.jobscheduler.app.JobSchedulerApplication;
import com.example.jobscheduler.service.ServiceManager;
import com.example.jobscheduler.task.LoadNewDataTask;
import com.example.jobscheduler.task.ThreadTaskExecutor;

public class BackgroundService extends Service {

    //Executor helps doing background work
    private ThreadTaskExecutor taskExecutor;

    //Task is used to process the data
    private LoadNewDataTask loadNewDataTask;

    //Used to read service state
    private ServiceManager serviceManager;

    @Override
    public void onCreate() {
        //Dependency injection
        JobSchedulerApplication app = JobSchedulerApplication.get(getBaseContext());
        this.loadNewDataTask = app.provideLoadNewDataTask();
        this.taskExecutor = app.provideThreadTaskExecutor();
        this.serviceManager = app.provideServiceManager();
        Log.i("BackgroundService", "onCreate");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("BackgroundService", "onStartCommand");
        //Do not execute tasks if service has been recently stopped
        if(!serviceManager.isScheduled()) return START_NOT_STICKY;
        //This function is called in main thread so we need to execute task in background thread.
        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                //Do the task
                Log.i("BackgroundService", "execute task");
                try {
                    loadNewDataTask.execute();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.i("BackgroundService", "stopSelf");
                //Call finish to release system resources
                stopSelf();
            }
        });
        // If we get killed, after returning from here, restart
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // We don't provide binding, so return null
        return null;
    }

    @Override
    public void onDestroy() {
        Log.i("BackgroundService", "onDestroy");
    }

}
