package com.example.jobscheduler.service;

import android.os.Build;

import com.example.jobscheduler.service.background.BackgroundManager;
import com.example.jobscheduler.service.job.JobSchedulerManager;
import com.example.jobscheduler.service.worker.WorkerServiceManager;

public class ServiceManager implements IBackgroundServiceManager {

    private final WorkerServiceManager workerServiceManager;

    private final JobSchedulerManager jobSchedulerManager;

    private final BackgroundManager backgroundManager;

    public ServiceManager(WorkerServiceManager workerServiceManager,
                          JobSchedulerManager jobSchedulerManager,
                          BackgroundManager backgroundManager) {
        this.workerServiceManager = workerServiceManager;
        this.jobSchedulerManager = jobSchedulerManager;
        this.backgroundManager = backgroundManager;
    }

    @Override
    public void schedule() {
        if(isKitKatApi()) {
            backgroundManager.schedule();
        } else if(isLollipopApi()) {
            jobSchedulerManager.schedule();
        } else if(isOreoApi()) {
            workerServiceManager.schedule();
        }
    }

    @Override
    public boolean isScheduled() {
        if(isKitKatApi()) {
            return backgroundManager.isScheduled();
        } else if(isLollipopApi()) {
            return jobSchedulerManager.isScheduled();
        } else { //isOreoApi
            return workerServiceManager.isScheduled();
        }
    }

    @Override
    public void stop() {
        if(isKitKatApi()) {
            backgroundManager.stop();
        } else if(isLollipopApi()) {
            jobSchedulerManager.stop();
        } else if(isOreoApi()) {
            workerServiceManager.stop();
        }
    }

    private boolean isKitKatApi() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP;
    }

    private boolean isLollipopApi() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP && Build.VERSION.SDK_INT < Build.VERSION_CODES.O;
    }

    private boolean isOreoApi() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.O;
    }

}
