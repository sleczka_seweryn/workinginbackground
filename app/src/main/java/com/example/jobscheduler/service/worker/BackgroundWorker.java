package com.example.jobscheduler.service.worker;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.example.jobscheduler.app.JobSchedulerApplication;
import com.example.jobscheduler.task.LoadNewDataTask;

import java.io.IOException;

public class BackgroundWorker extends Worker {

    //Task is used to process the data
    private final LoadNewDataTask loadNewDataTask;

    public BackgroundWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
        loadNewDataTask = JobSchedulerApplication.get(context).provideLoadNewDataTask();
        Log.i("BackgroundWorker", "onCreate");
    }

    @NonNull
    @Override
    public Result doWork() {
        try {
            Log.i("BackgroundWorker", "doWork");
            loadNewDataTask.execute();
            return Result.success();
        } catch (IOException ex) {
            Log.i("BackgroundWorker", "onError, retry");
            return Result.retry();
        } catch (Exception ex) {
            Log.i("BackgroundWorker", "onError");
            return Result.failure();
        }
    }
}
