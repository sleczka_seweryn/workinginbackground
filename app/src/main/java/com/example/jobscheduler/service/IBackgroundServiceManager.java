package com.example.jobscheduler.service;

public interface IBackgroundServiceManager {

    boolean isScheduled();

    void schedule();

    void stop();

}
