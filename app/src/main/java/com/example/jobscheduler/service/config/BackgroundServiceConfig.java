package com.example.jobscheduler.service.config;

public class BackgroundServiceConfig {

    private final long interval;

    public BackgroundServiceConfig(long interval) {
        this.interval = interval;
    }

    public long getInterval() {
        return interval;
    }
}
