package com.example.jobscheduler.service.worker;

import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.RequiresApi;

import androidx.work.PeriodicWorkRequest;
import androidx.work.WorkManager;

import com.example.jobscheduler.service.IBackgroundServiceManager;
import com.example.jobscheduler.service.config.BackgroundServiceConfig;

import java.time.Duration;
import java.util.UUID;

public class WorkerServiceManager implements IBackgroundServiceManager {

    public static final String WORK_REQUEST_ID = "com.example.jobscheduler.WORK_REQUEST_ID";

    private final SharedPreferences sharedPreferences;

    private final BackgroundServiceConfig config;

    public WorkerServiceManager(SharedPreferences sharedPreferences, BackgroundServiceConfig config) {
        this.sharedPreferences = sharedPreferences;
        this.config = config;
    }

    @Override
    public boolean isScheduled() {
        WorkManager mWorkManager = WorkManager.getInstance();
        String key = sharedPreferences.getString(WORK_REQUEST_ID,null);
        if(key == null) return false;
        return !mWorkManager.getWorkInfoById(UUID.fromString(key)).isDone();
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void schedule() {
        WorkManager mWorkManager = WorkManager.getInstance();
        PeriodicWorkRequest mRequest = new PeriodicWorkRequest
                .Builder(BackgroundWorker.class, Duration.ofMillis(config.getInterval()))
                .build();
        sharedPreferences.edit()
                .putString(WORK_REQUEST_ID, mRequest.getId().toString())
                .apply();
        mWorkManager.enqueue(mRequest);
    }

    @Override
    public void stop() {
        WorkManager mWorkManager = WorkManager.getInstance();
        String key = sharedPreferences.getString(WORK_REQUEST_ID,null);
        if(key == null) return;
        mWorkManager.cancelWorkById(UUID.fromString(key));
    }
}
