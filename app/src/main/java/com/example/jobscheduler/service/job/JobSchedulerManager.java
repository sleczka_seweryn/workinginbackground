package com.example.jobscheduler.service.job;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.example.jobscheduler.service.IBackgroundServiceManager;
import com.example.jobscheduler.service.config.BackgroundServiceConfig;

import java.util.List;

public class JobSchedulerManager implements IBackgroundServiceManager {

    private static final long MIN_INTERVAL_DURATION = 15*60*1000;

    private static final int BACKGROUND_SERVICE_ID_CODE = 2020;

    private final Context applicationContext;

    private final BackgroundServiceConfig config;

    public JobSchedulerManager(Context applicationContext, BackgroundServiceConfig config) {
        this.applicationContext = applicationContext;
        this.config = config;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public boolean isScheduled() {
        JobScheduler jobScheduler = (JobScheduler)applicationContext
                .getSystemService(Context.JOB_SCHEDULER_SERVICE);
        List<JobInfo> jobs = jobScheduler.getAllPendingJobs();
        for(JobInfo job: jobs) {
            if(job.getId() == BACKGROUND_SERVICE_ID_CODE) {
                return true;
            }
        }
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void schedule() {
        JobScheduler jobScheduler = (JobScheduler)applicationContext
                .getSystemService(Context.JOB_SCHEDULER_SERVICE);
        ComponentName componentName = new ComponentName(
                applicationContext,
                JobSchedulerService.class
        );
        JobInfo jobInfo = new JobInfo.Builder(BACKGROUND_SERVICE_ID_CODE, componentName)
                .setPeriodic(getIntervalDuration())
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)
                .setPersisted(true)
                .build();
        jobScheduler.schedule(jobInfo);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void stop() {
        JobScheduler jobScheduler = (JobScheduler)applicationContext
                .getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.cancel(BACKGROUND_SERVICE_ID_CODE);
    }

    private long getIntervalDuration() {
        if(config.getInterval() < MIN_INTERVAL_DURATION) {
            return MIN_INTERVAL_DURATION;
        }
        return config.getInterval();
    }

}
