package com.example.jobscheduler.service.job;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.example.jobscheduler.app.JobSchedulerApplication;
import com.example.jobscheduler.service.ServiceManager;
import com.example.jobscheduler.task.LoadNewDataTask;
import com.example.jobscheduler.task.ThreadTaskExecutor;

/**
 * This class is doing scheduled job in background thread.
 */
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class JobSchedulerService extends JobService {

    //Handler is used to deliver event from background to main thread
    private final Handler handler = new Handler(Looper.getMainLooper());

    //Executor helps doing background work
    private ThreadTaskExecutor taskExecutor;

    //Task is used to process the data
    private LoadNewDataTask loadNewDataTask;

    //Used to read service state
    private ServiceManager serviceManager;

    @Override
    public void onCreate() {
        //Dependency injection
        JobSchedulerApplication app = JobSchedulerApplication.get(getBaseContext());
        this.loadNewDataTask = app.provideLoadNewDataTask();
        this.taskExecutor = app.provideThreadTaskExecutor();
        this.serviceManager = app.provideServiceManager();
        Log.i("JobSchedulerService", "onCreate");
    }

    @Override
    public boolean onStartJob(final JobParameters params) {
        Log.i("JobSchedulerService", "onStartJob");
        //Do not execute tasks if service has been recently stopped
        if(!serviceManager.isScheduled()) return false;
        //This function is called in main thread so we need to execute task in background thread.
        taskExecutor.execute(new Runnable() {
            @Override
            public void run() {
                //Do the task
                Log.i("JobSchedulerService", "execute task");
                boolean hasError = false;
                try {
                    loadNewDataTask.execute();
                } catch (Exception e) {
                    Log.i("JobSchedulerService", "onError");
                    e.printStackTrace();
                    hasError = true;
                }
                //Call finish to release system resources
                callJobFinished(params, hasError);
            }
        });
        //Return true because job is not finished
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.i("JobSchedulerService", "onStopJob");
        //This function is called when job is premature terminated
        return false;
    }

    //Call JobFinish in main thread
    private void callJobFinished(final JobParameters param, final boolean hasError) {
        Log.i("JobSchedulerService", "jobFinished");
        handler.post(new Runnable() {
            @Override
            public void run() {
                jobFinished(param, hasError);
            }
        });
    }

}
