package com.example.jobscheduler.service.background;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.example.jobscheduler.service.IBackgroundServiceManager;
import com.example.jobscheduler.service.config.BackgroundServiceConfig;

import java.util.Calendar;

public class BackgroundManager implements IBackgroundServiceManager {

    private static final int BACKGROUND_SERVICE_ID_CODE = 2020;

    private final Context applicationContext;

    private final BackgroundServiceConfig config;

    public BackgroundManager(Context applicationContext,
                             BackgroundServiceConfig config) {
        this.applicationContext = applicationContext;
        this.config = config;
    }

    @Override
    public boolean isScheduled() {
        return PendingIntent.getService(
                applicationContext,
                BACKGROUND_SERVICE_ID_CODE,
                new Intent(applicationContext, BackgroundService.class),
                PendingIntent.FLAG_NO_CREATE
        ) != null;
    }

    @Override
    public void schedule() {
        PendingIntent pendingIntent = createBackgroundServiceIntent();
        AlarmManager alarmManager = (AlarmManager) applicationContext.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                Calendar.getInstance().getTimeInMillis(),
                config.getInterval(),
                pendingIntent
        );
    }

    @Override
    public void stop() {
        PendingIntent pendingIntent = createBackgroundServiceIntent();
        AlarmManager alarmManager = (AlarmManager) applicationContext.getSystemService(Context.ALARM_SERVICE);
        alarmManager.cancel(pendingIntent);
    }

    private PendingIntent createBackgroundServiceIntent() {
        return PendingIntent.getService(
                applicationContext,
                BACKGROUND_SERVICE_ID_CODE,
                new Intent(applicationContext, BackgroundService.class),
                PendingIntent.FLAG_CANCEL_CURRENT
        );
    }

}
