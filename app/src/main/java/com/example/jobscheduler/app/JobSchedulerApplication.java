package com.example.jobscheduler.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.example.jobscheduler.database.PostRepository;
import com.example.jobscheduler.log.LogRepository;
import com.example.jobscheduler.network.LoadPostsRequest;
import com.example.jobscheduler.service.ServiceManager;
import com.example.jobscheduler.service.background.BackgroundManager;
import com.example.jobscheduler.service.config.BackgroundServiceConfig;
import com.example.jobscheduler.service.job.JobSchedulerManager;
import com.example.jobscheduler.service.worker.WorkerServiceManager;
import com.example.jobscheduler.task.LoadNewDataTask;
import com.example.jobscheduler.task.ThreadTaskExecutor;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;


public class JobSchedulerApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static JobSchedulerApplication get(Context context) {
        return (JobSchedulerApplication)context.getApplicationContext();
    }

    public OkHttpClient provideOkHttpClient() {

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();
    }

    public SharedPreferences provideSharedPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(this);
    }

    public LoadPostsRequest provideLoadPostsRequest() {
        return new LoadPostsRequest(provideOkHttpClient());
    }

    public PostRepository providePostRepository() {
        return new PostRepository();
    }

    public ThreadTaskExecutor provideThreadTaskExecutor() {
        return new ThreadTaskExecutor();
    }

    public LoadNewDataTask provideLoadNewDataTask() {
        return new LoadNewDataTask(
                provideLoadPostsRequest(),
                providePostRepository(),
                provideLogRepository()
        );
    }

    public BackgroundServiceConfig provideBackgroundServiceConfig() {
        return new BackgroundServiceConfig(15*60*1000);
    }

    public WorkerServiceManager provideWorkerServiceManager() {
        return new WorkerServiceManager(
                provideSharedPreferences(),
                provideBackgroundServiceConfig()
        );
    }

    public JobSchedulerManager provideJobSchedulerManager() {
        return new JobSchedulerManager(
                this,
                provideBackgroundServiceConfig()
        );
    }

    public BackgroundManager provideBackgroundManager() {
        return new BackgroundManager(
                this,
                provideBackgroundServiceConfig()
        );
    }

    public ServiceManager provideServiceManager() {
        return new ServiceManager(
                provideWorkerServiceManager(),
                provideJobSchedulerManager(),
                provideBackgroundManager()
        );
    }

    public LogRepository provideLogRepository() {
        return new LogRepository(provideSharedPreferences());
    }

}
