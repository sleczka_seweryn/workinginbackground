package com.example.jobscheduler.network;

import com.example.jobscheduler.data.PostDto;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class LoadPostsRequest {

    private final OkHttpClient okHttpClient;

    public LoadPostsRequest(OkHttpClient okHttpClient) {
        this.okHttpClient = okHttpClient;
    }

    public List<PostDto> call( String argument ) throws IOException {
        Request request = new Request.Builder()
                .url("http://jsonplaceholder.typicode.com/posts")
                .build();

        try (Response response = okHttpClient.newCall(request).execute()) {
            PostDto[] postsArray = new Gson().fromJson(response.body().string(), PostDto[].class);
            return new ArrayList<PostDto>(Arrays.asList(postsArray));
        }
    }

}
