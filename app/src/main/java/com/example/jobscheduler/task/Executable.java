package com.example.jobscheduler.task;

public interface Executable {

    void execute() throws Exception;

}
