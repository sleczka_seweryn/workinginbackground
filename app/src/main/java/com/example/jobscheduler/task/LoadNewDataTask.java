package com.example.jobscheduler.task;

import com.example.jobscheduler.data.PostDto;
import com.example.jobscheduler.database.PostRepository;
import com.example.jobscheduler.log.LogRepository;
import com.example.jobscheduler.network.LoadPostsRequest;

import java.util.List;

public class LoadNewDataTask implements Executable {

    private final LoadPostsRequest loadPostsRequest;

    private final PostRepository postRepository;

    private final LogRepository logRepository;

    public LoadNewDataTask(LoadPostsRequest loadPostsRequest, PostRepository postRepository, LogRepository logRepository) {
        this.loadPostsRequest = loadPostsRequest;
        this.postRepository = postRepository;
        this.logRepository = logRepository;
    }

    @Override
    public void execute() throws Exception {
        //Load posts from backend
        List<PostDto> newTasks = loadPostsRequest.call("some arguments");
        //Save posts in database
        postRepository.savePosts(newTasks);
        //Log complete event
        logRepository.log("LoadNewDataTask:complete");
    }

}


