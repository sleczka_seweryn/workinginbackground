package com.example.jobscheduler.task;

import java.util.concurrent.Executor;

public class ThreadTaskExecutor implements Executor {

    @Override
    public void execute(Runnable command) {
        new Thread(command).start();
    }

}
