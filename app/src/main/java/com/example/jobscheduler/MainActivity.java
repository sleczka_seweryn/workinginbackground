package com.example.jobscheduler;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.example.jobscheduler.app.JobSchedulerApplication;
import com.example.jobscheduler.log.LogListAdapter;
import com.example.jobscheduler.log.LogRepository;
import com.example.jobscheduler.service.ServiceManager;

public class MainActivity extends AppCompatActivity {

    public ServiceManager serviceManager;

    public Button scheduleButton;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager layoutManager;

    private LogRepository logRepository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        JobSchedulerApplication app = JobSchedulerApplication.get(getBaseContext());
        serviceManager = app.provideServiceManager();
        logRepository = app.provideLogRepository();

        scheduleButton = findViewById(R.id.schedule_button);
        if(serviceManager.isScheduled()) {
            scheduleButton.setText("STOP service");
        } else {
            scheduleButton.setText("START service");
        }

        scheduleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(serviceManager.isScheduled()) {
                    serviceManager.stop();
                    scheduleButton.setText("START service");
                } else {
                    serviceManager.schedule();
                    scheduleButton.setText("STOP service");
                }
            }
        });

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        mAdapter = new LogListAdapter(logRepository.list());
        recyclerView.setAdapter(mAdapter);
    }

}