package com.example.jobscheduler.data;

public class PostDto {

    public final String userId;

    public final String id;

    public final String title;

    public final String body;

    public PostDto(String userId, String id, String title, String body) {
        this.userId = userId;
        this.id = id;
        this.title = title;
        this.body = body;
    }

    public String getUserId() {
        return userId;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }
}
